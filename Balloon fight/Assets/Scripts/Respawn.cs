﻿using UnityEngine;
using System.Collections;

public class Respawn : MonoBehaviour {

	// Use this for initialization
	GameObject [] respawns;
	Vector3[] spawn_position;
	float timer;
	Animator my_anim;
	Hit my_hit;

	void Start () {
		my_hit = gameObject.GetComponent<Hit> ();
		my_anim = gameObject.GetComponent<Animator> ();
		respawns = GameObject.FindGameObjectsWithTag ("Respawn");
		spawn_position = new Vector3[respawns.Length];
		for (int i = 0; i < respawns.Length; i++) {
			spawn_position [i] = respawns [i].transform.position;
		}


	}
	
	// Update is called once per frame

	void Update () {

		if(my_anim.GetCurrentAnimatorStateInfo(0).IsName("Dead")){
			timer = timer + Time.deltaTime;
			if (timer > 10.0f) 
			{
				RespawnFunc ();
				timer = 0f;
			}
		}
	}


	void RespawnFunc()
	{
		int pos = Random.Range(0,respawns.Length);
		//Debug.Log (pos);
		gameObject.transform.position = spawn_position [pos];
		my_hit.setHealth (my_hit.maxHealth);
		my_hit.setDead (false);
		my_anim.SetBool ("Dead", false);
		my_hit.setDeath (false);
		my_anim.Play("Grounded");
	}

	 
}
