﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class AgentScript : MonoBehaviour {

	public float hit_distance;
	public float hunt_distance;
	public int run_for_Health;
	private float speed;
	private Animator animator;
	private GameObject[] obj;
	private GameObject[] enemies;
	private GameObject closest_enemy;
	private GameObject closest_buff;
	private GameObject[] healthBuffs;
	private GameObject bufferSpark;
	private Animator[] animators;
	private Transform player; 
	private Hit hitScript;
	private BuffController bControl;
	private NavMeshAgent agent;
	private bool hit;




	void Start () 
	{
		bControl = GameObject.FindGameObjectWithTag ("BuffController").GetComponent<BuffController>();
		player = GetComponent<Transform> ();
		hitScript = GetComponent<Hit> ();
		animator = GetComponent<Animator> ();
		agent = GetComponent<NavMeshAgent> ();	
		healthBuffs = GameObject.FindGameObjectsWithTag ("HealthBuff");
		obj = GameObject.FindGameObjectsWithTag ("Enemy");
		enemies = new GameObject[obj.Length];
		animators = new Animator[obj.Length];

		closest_enemy = null;
		closest_buff = null;
		hit = false;
		speed = agent.speed;
		animator.SetFloat ("Forward",0.0f);
		//AI sets itself to null in the list of enemies
		for (int i=0;i<obj.Length;i++)
		{
			enemies[i] = obj[i].transform.root.gameObject;
			animators [i] = enemies [i].GetComponent<Animator> ();
			if (player.position == enemies [i].transform.position)
				enemies [i] = null;
			//Debug.Log (enemies [i].gameObject.name);
		}

	}





	// Update is called once per frame
	void Update ()
	{
		//if only scale buff is available

		if (bControl.getCounter() >  1) 
		{
			if (hitScript.getHealth () < run_for_Health && hitScript.getHealth () > 0)
			{
				getClosestBuff ();
				//if there is a buff nearby
				if (closest_buff != null )
				{
					setupAgent (closest_buff.transform.position);
					return;				
				}
			}
		}


		if (enemies != null) 
		{ 
			//if there is no enemy selected or the previous selected closest enemy is too far away to hunt
			if(!enemyWorthHunting())
				getClosestEnemy ();

			//if there is no enemy nearby or if the AI character is dead
			if (closest_enemy == null || animator.GetBool("Dead")) 
			{
				//stop hunting 
				agent.ResetPath();
				SetAnimator ();
				return;
			}


			if (Vector3.Distance (player.transform.position, closest_enemy.transform.position) < hit_distance) 
			{
				hit = true;
				RotateTowards (closest_enemy.transform);
			}

			setupAgent (closest_enemy.transform.position);
			hit = false;
		}

	}

	void setupAgent(Vector3 pos)
	{
		agent.SetDestination (pos);
		SetAnimator ();
		agent.speed = speed;	
	}


	void getClosestBuff()
	{
		closest_buff = null;
		float min_distance = float.MaxValue;
		float tmp_distance ;

		for (int i = 0; i < healthBuffs.Length; i++) 
		{
			//if the buff is not active search for another
			if (healthBuffs[i] == null || (!healthBuffs [i].activeSelf))
				continue;

			tmp_distance = Vector3.Distance (player.position, healthBuffs[i].transform.position);

			if (tmp_distance < min_distance) 
			{
				min_distance = tmp_distance;
				closest_buff = healthBuffs [i];
			}
		}
	}


	//find the closest enemy 
	void getClosestEnemy()
	{
		closest_enemy = null;
		float min_distance = float.MaxValue;
		float tmp_distance ;

		for (int i = 0; i < enemies.Length; i++) 
		{
			//if the enemy is not active search for another
			if (enemies[i] == null || animators[i].GetBool("Dead"))
				continue;

			tmp_distance = Vector3.Distance (player.position, enemies [i].transform.position);

			if (tmp_distance < min_distance) 
			{
				min_distance = tmp_distance;
				closest_enemy = enemies [i];
			}
		}
	}







	//check if enemy is worth hunting
	bool enemyWorthHunting()
	{	
		float distance;
		//many ifs so that there is a precedence in the requirements to hunt someone

		//if enemy is dead or there is no enemy nearby
		if (closest_enemy == null || closest_enemy.GetComponent<Animator> ().GetBool("Dead"))
			return false;

		distance = Vector3.Distance (transform.position, closest_enemy.transform.position);
		//if enemy hp is too low (below 1/5 of his health) 
		if (distance < hunt_distance * 3 && closest_enemy.GetComponent<Hit> ().getHealth() < closest_enemy.GetComponent<Hit> ().maxHealth / 5)
			return true;

		//if enemy too far away
		if(distance > hunt_distance)
			return false;

		return true;
	}






	void SetAnimator()
	{
		//if hit is true the character attacks
		animator.SetBool ("Click", hit);
		//according to how forward he moved the animation parameter changes its values
		if(agent.velocity.z > 0)
			animator.SetFloat ("Forward",agent.velocity.z);
		else
			animator.SetFloat ("Forward",-agent.velocity.z);
	}

	private void RotateTowards (Transform target)
	{
		Vector3 direction = (target.position - transform.position).normalized;
		Quaternion lookRotation = Quaternion.LookRotation (direction);
		transform.rotation = Quaternion.Slerp (transform.rotation, lookRotation, Time.deltaTime * agent.angularSpeed);
	}


	void OnTriggerEnter(Collider other) 
	{
		if (other.gameObject.CompareTag ("Buff")) 
		{
			//an uparxoun polla buff tha psaxnoume ton kontinotero emitter h apla tha valoume diaforetika 
			//tags ston kathenan
			other.gameObject.SetActive (false);
			bufferSpark = GameObject.FindGameObjectWithTag ("Emitter");
			bufferSpark.GetComponent<EllipsoidParticleEmitter> ().enabled = true;	
			bufferSpark.GetComponent<SetupSpark> ().player = gameObject;
			bControl.decreaseCounter ();
		}
		else if (other.gameObject.CompareTag ("HealthBuff")) 
		{
			other.gameObject.SetActive (false);
			increaseHealth ();
			bControl.decreaseCounter ();
		}
	}

	void increaseHealth()
	{
		int health = hitScript .getHealth();
		if (health + 20 > hitScript .maxHealth)
			hitScript .setHealth (hitScript .maxHealth);
		else
			hitScript .setHealth(health + 20);
	}

}
