﻿using UnityEngine;
using System.Collections;

public class Controller : MonoBehaviour {

	public int speed;
	private Rigidbody rb;

	void Start () {
		rb = GetComponent<Rigidbody> (); 		
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		float horizontal = Input.GetAxis ("Horizontal");
		float vertical = Input.GetAxis ("Vertical");

		Vector3 vect = new Vector3 (horizontal, 0.0f, vertical);
		rb.AddForce (speed * vect);
	}
}
