﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UpdateUI : MonoBehaviour {

	GameObject[] playersScore;
	public static string[] namesScore;
	public static string[] killsScore;
	public Camera cam;
	private Text text_kills;
	private Text text_health;
	private int kills;
	private int health;
	private Text text_timer;
	private string minutes;
	private string seconds;
	public float timer;//Countdown Timer
	private GameObject[] names;
	private string[] botNames = new string[16] {"BotJohn","BotThemistokles","BotMarios","BotMathew","BotNick","BotBill",
												"BotAlexander","BotMike","BotGeorge","BotKleanthis","BotAgni","BotTormund",
												"BotSnow","BotBolton","BotDrogo","BotElliot"};


	// Use this for initialization
	void Start () 
	{
		text_kills = GameObject.Find ("Kills").GetComponent<Text> ();
		text_health = GameObject.Find ("Health").GetComponent<Text> ();
		text_timer = GameObject.Find ("Timer").GetComponent<Text> ();
		playersScore = GameObject.FindGameObjectsWithTag ("Enemy");
		timer = MenuScript.time;
		namesScore = new string[playersScore.Length];
		killsScore = new string[playersScore.Length];

		names = GameObject.FindGameObjectsWithTag ("name");

		for (int j = 0; j < names.Length; j++)
			if(!names[j].transform.root.CompareTag("Player"))
			{
				names [j].GetComponent<TextMesh> ().text = botNames [j];
				names [j].GetComponent<TextMesh> ().color = new Color (1, 0.2f, 0.2f, 1);
			}
			else
				names [j].GetComponent<TextMesh> ().color = new Color (0, 0, 1, 1);
	}


	// Update is called once per frame
	void Update () {

		if (Input.GetKey ("escape")) {
			Time.timeScale = 0;
			for (int i = 0; i < playersScore.Length; i++)
				playersScore [i] = playersScore [i].transform.root.gameObject;


			for (int i = 0; i < playersScore.Length; i++) {
				killsScore [i] = playersScore [i].GetComponent<Hit> ().getKills ().ToString();
				namesScore [i] = playersScore [i].transform.FindChild("name").GetComponent<TextMesh> ().text;
			}
			SceneManager.LoadScene (2);
		}
		timer -= Time.deltaTime;
		if((int)(timer / 60) < 10)
			minutes = "0" + (int)(timer / 60);
		else
			minutes = "" + (int)(timer / 60);

		if((int)(timer % 60) < 10)
			seconds = "0" + (int)(timer % 60);
		else
			seconds = "" + (int)(timer % 60);

		text_timer.text = "" + minutes + ":" + seconds;
		if (timer <= 0) {

			Time.timeScale = 0;
			for (int i = 0; i < playersScore.Length; i++)
				playersScore [i] = playersScore [i].transform.root.gameObject;


			for (int i = 0; i < playersScore.Length; i++) {
				killsScore [i] = playersScore [i].GetComponent<Hit> ().getKills ().ToString();
				namesScore [i] = playersScore [i].transform.FindChild("name").GetComponent<TextMesh> ().text;
			}
			SceneManager.LoadScene (2);

		}
		
		for (int i = 0; i < names.Length; i++) 
		{
			names [i].transform.LookAt (cam.transform.position);
			names [i].transform.Rotate (0.0f, 180.0f, 0.0f);

		}
		kills = gameObject.GetComponent<Hit> ().getKills();
		health = gameObject.GetComponent<Hit> ().getHealth();
		text_kills.text =  kills.ToString();
		text_health.text =  health.ToString();
	}

//	public string [] getNames()
//	{
//		return namesScore;
//	}
//
//	public string [] getKills()
//	{
//		return killsScore;
//	}
}
	
