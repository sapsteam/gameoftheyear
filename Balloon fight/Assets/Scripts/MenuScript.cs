﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class MenuScript : MonoBehaviour {

	public static string playerName;
	public static float time;
    public Button startButton;
    public Button exitButton;
    public Button optionsButon;
	public Button soundButton;
	public Button backButton;
	public Button go;
	public Button backFromCreate;
	public Canvas options;
	public Canvas main;
	public Canvas create;
	public InputField nameInput;
	public InputField timerInput;
	public AudioSource audio;

    // Use this for initialization
    void Start () {
		playerName = "";
        startButton = startButton.GetComponent<Button>();
        exitButton = exitButton.GetComponent<Button>();
        optionsButon = optionsButon.GetComponent<Button>();
		soundButton = soundButton.GetComponent<Button> ();
		backButton = backButton.GetComponent<Button> ();
		go = go.GetComponent<Button> ();
		backFromCreate = backFromCreate.GetComponent<Button> ();
		options = options.GetComponent<Canvas> ();
		audio = audio.GetComponent<AudioSource> ();
		main = main.GetComponent<Canvas> ();
		create = create.GetComponent<Canvas> ();
		nameInput = nameInput.GetComponent<InputField> ();
		timerInput = timerInput.GetComponent<InputField> ();
		options.enabled = false;
		create.enabled = false;
	}
	
    public void StartLevel()
    {
		if (!nameInput.text.Equals(""))
			playerName = nameInput.text;
		else
			playerName = "Player";
		if (!timerInput.text.Equals (""))
			time = float.Parse (timerInput.text) * 60;
		else
			time = 10.00f * 60;
        SceneManager.LoadScene(1);
    }
	
    public void ExitGame()
    {
        Application.Quit();
    }

	public void ShowOptions(){
		main.enabled = false;
		options.enabled = true;
	}

	public void ShowCreate(){
		main.enabled = false;
		create.enabled = true;
	}

	public void ReturnToMainFromCreate(){
		create.enabled = false;
		main.enabled = true;
	}

	public void ReturnToMainMenu(){
		options.enabled = false;
		main.enabled = true;
	}

	public void SoundToggle(){
		if (audio.isPlaying) {
			audio.Stop ();
		} else {
			audio.Play ();
		}
	}
}
