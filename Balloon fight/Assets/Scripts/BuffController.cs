﻿using UnityEngine;
using System.Collections;

public class BuffController : MonoBehaviour {

	private int activeBuffs;
	private int numberOfBuffs;
	private float[] timers;
	private float deltaTime;
	private GameObject strongBuff;
	private GameObject[] healthBuffs;
	// Use this for initialization
	void Start () 
	{
		healthBuffs = GameObject.FindGameObjectsWithTag ("HealthBuff");
		numberOfBuffs = healthBuffs.Length;
	 

		//+ 1 to fit the scale buff
		timers = new float[numberOfBuffs + 1];
		for (int i = 0; i < numberOfBuffs + 1; i++)
			timers [i] = 0;

		strongBuff = transform.Find ("ScaleBuff").gameObject;
		activeBuffs = numberOfBuffs;
			
	}
	 
	void Update ()
	{
		deltaTime = Time.deltaTime;

		//check if taken buffs must respawn and count the remaining time for the respawn
		for (int i = 0; i < numberOfBuffs; i++) 
			if (!healthBuffs [i].activeSelf)
				updateBuffTimer (healthBuffs [i], i, 60.0f);
		
		if (!strongBuff.activeSelf)
			updateBuffTimer (strongBuff, numberOfBuffs - 1, 60);
	}



	void updateBuffTimer(GameObject buff,int buffNo, float Offset)
	{
		timers [buffNo] += deltaTime;

		if (timers [buffNo] > Offset) 
		{
			buff.SetActive (true);
			timers [buffNo] = 0.0f;
			activeBuffs++;
		}
			
	}

	public void decreaseCounter()
	{
		activeBuffs--;
	}

	public int getCounter()
	{
		return activeBuffs;
	}
}
