﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class ModelChange : MonoBehaviour {

	private GameObject player;
	private GameObject fat;

	void Start(){
		player = GameObject.Find("Player");
		fat = GameObject.Find ("fat");
	}
		

	void OnTriggerEnter(Collider col)
	{
		if(col.GetComponent<Collider>().name == "ScaleBuff")
		{
			PrefabUtility.ReplacePrefab (fat, player);
		}
	}

}
