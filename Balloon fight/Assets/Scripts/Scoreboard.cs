﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;

public class Scoreboard : MonoBehaviour {
	GameObject[] scoreline;
	GameObject temp;
	string [] names;
	string [] kills;
	Text [] nameLine;
	Text [] killsLine;
	string tmp,tmp2;
	public Button menu;

	// Use this for initialization
	void Start () {
		scoreline = GameObject.FindGameObjectsWithTag ("ScoreLine");
		nameLine = new Text[scoreline.Length];
		killsLine = new Text[scoreline.Length];
		menu = menu.GetComponent<Button> ();
		names = UpdateUI.namesScore;
		kills = UpdateUI.killsScore;


	
		for (int i = 0; i < scoreline.Length; i++) {
			nameLine [i] = scoreline [i].transform.FindChild ("player").GetComponent<Text> ();
			killsLine [i] = scoreline [i].transform.FindChild ("kills").GetComponent<Text> ();
		}





		for (int i = 0; i < kills.Length; i++)
		{
			for (int j = 0; j < kills.Length - 1; j++)
			{
				if (Int32.Parse(kills[j]) < Int32.Parse(kills[j + 1]))
				{
					tmp = kills[j + 1];
					kills[j+ 1] = kills[j];
					kills[j] = tmp;

					tmp2 = names[j + 1];
					names[j+ 1] = names[j];
					names[j] = tmp2;
				}       
			} 
		}



	
		for (int i = 0; i < kills.Length; i++) {
			for (int j = 0; j < kills.Length; j++) {
				if (Int32.Parse (nameLine [j].transform.parent.name) == i) {
					nameLine [j].text = names [i];
					killsLine [j].text = kills [i];
				}
			}
		}
	
	}

	public void BackToMenu(){
		Application.Quit();
	}

	// Update is called once per framew
	void Update () {
		
	
	}
}
