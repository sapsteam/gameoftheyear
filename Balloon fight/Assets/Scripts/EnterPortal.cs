﻿using UnityEngine;
using System.Collections;

public class EnterPortal : MonoBehaviour {

	public GameObject exitPortal;
	private Vector3 coordinates;
	// Use this for initialization

	void Start () 
	{	
		coordinates = exitPortal.transform.position;
	}
	
	void OnTriggerEnter(Collider other)
	{
		if (other.transform.Find ("Enemy") == null)
			return;

		other.transform.position = coordinates;
		
	}
}
