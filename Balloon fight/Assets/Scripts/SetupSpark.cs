﻿using UnityEngine;
using System.Collections;

public class SetupSpark : MonoBehaviour {

	Hit playerScript;
	EllipsoidParticleEmitter myEmitter;
	ParticleRenderer myRenderer;
	public GameObject player;
	private float timer;
	bool tookBuff;

	// Use this for initialization
	void Start () 
	{
		tookBuff = false;
		timer = 0.0f;
		myRenderer = GetComponent<ParticleRenderer> ();
		myEmitter = GetComponent<EllipsoidParticleEmitter>();
		myRenderer.enabled = false;
		myEmitter.enabled = false;
	}

	// Update is called once per frame
	void Update ()
	{
		if(!myEmitter.enabled)
			return;


		if (tookBuff) 
		{
			GetComponent<NavMeshAgent> ().SetDestination (player.transform.position);
			timer += Time.deltaTime;
			if (timer > 30.0f)
			{
				timer = 0.0f;
				myRenderer.enabled = false;
				myEmitter.enabled = false;
				player.transform.Find ("baby").localScale -= new Vector3 (0.2F, 0.3f, 0.5f);
				playerScript.setDamage (playerScript.getDamage() - 5);
				tookBuff = false;
			}
		}
		else
		{
			playerScript = player.GetComponent<Hit> ();
			myRenderer.enabled = true;
			player.transform.Find ("baby").localScale += new Vector3(0.2F, 0.3f, 0.5f);
			playerScript.setHealth (playerScript.maxHealth);
			playerScript.setDamage (playerScript.getDamage() + 5);
			tookBuff = true;
		}
	}
}


