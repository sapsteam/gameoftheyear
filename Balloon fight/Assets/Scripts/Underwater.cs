﻿using UnityEngine;
using System.Collections;

public class Underwater : MonoBehaviour {

	public float waterLevel;
	private Color normalColor;
	private Color underwaterColor;

	void Start () 
	{
		RenderSettings.fog = true;
		normalColor = new Color (0.5f, 0.5f, 0.5f, 0.5f);
		underwaterColor = new Color (0.3f, 0.41f, 0.57f, 0.1f);
	}
	
	// Update is called once per frame
	void Update () 
	{
		if (transform.position.y < waterLevel)
			SetUnderwater ();
		else
			SetNormal ();
	}

	void SetNormal()
	{
		RenderSettings.fogColor = normalColor;
		RenderSettings.fogDensity = 0.002f;
	}

	void SetUnderwater()
	{
		RenderSettings.fogColor = underwaterColor;
		RenderSettings.fogDensity = 0.010f;
	}
}
