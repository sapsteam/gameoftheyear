﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Hit : MonoBehaviour {
	
	Animator[] anim;
	GameObject[] obj;
	GameObject[] enemies;
	private int kills;
	public int maxHealth;
	private int health;
	public int damage;
	private bool dead;
	GameObject killer;
	private Text killlog1;
	private Text killlog2;
	private Text killlog3;
	private Text killlog4;

	private bool death;

	// Use this for initialization


	void Start () {

		dead = false;
		death = false; 
		obj = GameObject.FindGameObjectsWithTag("Enemy");
		anim = new Animator[obj.Length];
		enemies = new GameObject[obj.Length];
		health = maxHealth;
		kills = 0;

		killlog1 = GameObject.Find ("KillLog1").GetComponent<Text> ();
		killlog2 = GameObject.Find ("KillLog2").GetComponent<Text> ();
		killlog3 = GameObject.Find ("KillLog3").GetComponent<Text> ();
		killlog4 = GameObject.Find ("KillLog4").GetComponent<Text> ();

		killlog1.text = ""; 
		killlog2.text = "";
		killlog3.text = ""; 
		killlog4.text = ""; 

		for (int i=0;i<obj.Length;i++)
		{
			enemies[i] = obj[i].transform.root.gameObject;
			anim[i] = enemies[i].GetComponent<Animator> ();
			//Debug.Log (enemies [i].gameObject.name);
		}
			
	}

	// Update is called once per frame
	void Update () {
		if (!death) {
			if (dead) {
				death = true;
				killer.GetComponent<Hit>().kills +=1;
				Animator animat = GetComponent<Animator> ();
				animat.SetBool ("Dead",dead);
				killlog1.text = killlog2.text;
				killlog2.text = killlog3.text; 
				killlog3.text = killlog4.text;
				killlog4.text = killer.transform.Find("name").GetComponent<TextMesh>().text.ToString() + " ballooned " + gameObject.transform.Find("name").GetComponent<TextMesh>().text.ToString();
				transform.root.GetComponent<AudioSource> ().Play ();
			}
		}
	}

	void OnTriggerEnter(Collider other)
	{

		if (other.transform.root.position.Equals (transform.position))
			return;

		for (int i = 0; i < anim.Length; i++)
			if (other.transform.root.gameObject.name.Equals (enemies [i].name) && anim [i].GetCurrentAnimatorStateInfo (0).IsName ("Attack")) {
				while (anim [i].GetCurrentAnimatorStateInfo (0).normalizedTime < anim [i].GetCurrentAnimatorStateInfo (0).length - 1)
					return;

				//do not deal damage during the transition from attack to dead
				if(anim[i].GetBool("Dead"))
					return;

				other.GetComponent<AudioSource> ().Play ();
				health = health - other.transform.root.GetComponent<Hit> ().damage;
				if (health <= 0) {
					killer = other.transform.root.gameObject;
					dead = true;
				}


				break;
			}
		
		


	}



	public void setDeath(bool dth){
		death = dth;
	}

	public void setHealth(int hp){
		health = hp;
	}

	public void setDead(bool deadParam){
		dead = deadParam;
	}

	public void setDamage(int dmg){
		damage = dmg;
	}

	public int getHealth()
	{
		return health;
	}

	public int getKills()
	{
		return kills;
	}

	public int getDamage()
	{
		return damage;
	}

 
}
	
