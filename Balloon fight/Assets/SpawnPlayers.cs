﻿using UnityEngine;
using System.Collections;

public class SpawnPlayers : MonoBehaviour {

	GameObject[] players;
	GameObject[] respawns;
	// Use this for initialization
	void Start () 
	{
		players = GameObject.FindGameObjectsWithTag ("Enemy");
		for (int i = 0; i < players.Length; i++)
			players [i] = players [i].transform.root.gameObject;	

		respawns = GameObject.FindGameObjectsWithTag ("Respawn");
 

		for (int i = 0; i < players.Length; i++)
			players [i].transform.position = respawns [i].transform.position;
		
	
	}
	

}
